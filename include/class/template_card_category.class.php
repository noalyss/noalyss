<?php

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

require_once NOALYSS_INCLUDE.'/database/fiche_def_ref_sql.class.php';

/**
 * @file
 * @brief  Manage the template of card category 
 */

/**
 * @class Template_Card_Category
 * @brief Manage the template of card category
 */
class Template_Card_Category extends Manage_Table_SQL
{

    function __construct(Fiche_def_ref_SQL $p_table)
    {
        $this->table=$p_table;
        parent::__construct($p_table);
        // Label of the columns
        $this->set_col_label("frd_text", _("Nom"));
        $this->set_col_label("frd_class_base", _("Poste comptable de base"));
        $this->set_col_label("frd_id", _("ID"));
        // Cannot update frd_id
        $this->set_property_updatable("frd_id", FALSE);
        $this->a_order=["frd_id", "frd_text", "frd_class_base"];
    }

    function delete()
    {
        $cn=Dossier::connect();

        if ($cn->get_value("select count(*) from fiche_def where frd_id=$1",
                        [$this->table->frd_id])>0)
        {
            throw new Exception(_("Effacement impossible : catégorie utilisée"));
        }
        $cn->exec_sql("delete from attr_min where frd_id=$1",[$this->table->frd_id]);
        $this->table->delete();
    }

    /**
     * Check before inserting or updating, return TRUE if ok otherwise FALSE.
     * @return boolean
     */
    function check()
    {
        $cn=Dossier::connect();
        $error=0;
        if (trim($this->table->frd_text)=="")
        {
            $this->set_error("frd_text", _("Le nom ne peut pas être vide"));
            $error++;
        }
        if (trim($this->table->frd_class_base)!="")
        {
            $cnt=$cn->get_value("select count(*) from tmp_pcmn where pcm_val=$1"
                    , [$this->table->frd_class_base]);
            if ($cnt==0)
            {
                $this->set_error("frd_class_base",
                        _("Poste comptable n'existe pas"));
                $error++;
            }
        }

        if ($error!=0)
        {
            return false;
        }
        return true;
    }

    /**
     * @brief display into a dialog box the datarow in order 
     * to be appended or modified. Can be override if you need
     * a more complex form
     */
    function input()
    {
        echo "<br><font color=\"red\"> ";
        echo _("Attention, ne pas changer la signification de ce poste.");
        echo hi(_("par exemple ne pas changer Client par fournisseur"))."<br>";
        echo _("sinon le programme fonctionnera mal, ".
                "utiliser uniquement des chiffres pour la classe de base ou rien")."</font>";
        $error_name=$this->get_error("frd_text");
        $error_account=$this->get_error("frd_class_base");
        $error_name=($error_name=="")?"":HtmlInput::errorbulle($error_name);
        $error_account=($error_account=="")?"":HtmlInput::errorbulle($error_account);
        
        $frd_id=HtmlInput::hidden("frd_id",$this->get_table()->getp("frd_id"));
        $name=new IText("frd_text",$this->get_table()->getp("frd_text"));
        $account=new IPoste("frd_class_base",$this->get_table()->getp("frd_class_base"));
        $account->set_attribute('gDossier',Dossier::id());
        $account->set_attribute('jrn',0);
        $account->set_attribute('account','frd_class_base');
        $name_label=_("Nom");
        $account_label=_("Poste comptable de base");
       echo <<<EOF
        <table>
            <tbody>
                <tr>
                    <td> ID  </td>
                        <td>{$frd_id}{$this->get_table()->getp("frd_id")}</td>
                </tr>
                <tr>
                    <td> {$name_label}  {$error_name}</td>
                    <td>
                    {$name->input()}
                    </td>
                </tr>
                <tr>
                    <td> {$account_label} {$error_account}</td>
                        <td>
                            {$account->input()}
                    </td>
                </tr>
            </tbody>
        </table>
EOF;
       echo HtmlInput::get_to_hidden(["gDossier","op","p_id"]);
        /**
         * Add / Remove attribut Minimum
         */
        if ($this->table->frd_id!=-1)
        {
            echo h2(_("Attribut minimum pour les catégories de fiches"));
            $cn=Dossier::connect();
            $dossier_id=Dossier::id();
            $objname=$this->get_object_name();
            $a_attribut=$cn->get_array("select ad_id,ad_text,ad_type,a1.ad_default_order from attr_min a1 join attr_def using (ad_id) where frd_id=$1 order by a1.ad_default_order",
                    [$this->table->frd_id]);
            $nb_attribut=count($a_attribut);
            printf('<ul class="list-unstyled" style="width: 60%%;margin-left: 21%%" id="%s_list"> ', $objname);
            $used=$cn->get_value("select count(*) from jnt_fic_attr join fiche_def using (fd_id) where frd_id=$1",
                    [$this->table->frd_id]);
            if ($used!=0)
            {
                echo_warning( _("Catégorie utilisée, les attributs de base ne peuvent pas être modifiés"));
            }
            echo _("Vous pouvez modifier l'ordre des attributs avec la souris");
            for ($i=0; $i<$nb_attribut; $i++)
            {
                printf('<li id="%s_elt%d" style="cursor:move;border:1px solid navy;padding : 0.5rem 0.2rem 0.5rem 0.2rem;margin-top:2px">', $objname
                        , $a_attribut[$i]['ad_id']);
                echo $a_attribut[$i]['ad_text'];
                // cannot delete NAME and QUICKCODE + attribute used in a
                if (!in_array($a_attribut[$i]['ad_id'], [ATTR_DEF_NAME, ATTR_DEF_QUICKCODE])&&$used==0)
                {
                    // allow to remove attribute
                    $js=sprintf("category_card.remove_attribut('%s','%s','%s',%d)",
                            Dossier::id(), $this->table->frd_id, $objname, $a_attribut[$i]['ad_id']);
                    echo Icon_Action::trash(uniqid(), $js);
                }
                echo '</li>';
            }
            echo '</ul>';
            // Add some attribute if not used
            if ($used==0)
            {
                $sel_attribut=new ISelect("sel".$this->get_object_name());
                $sel_attribut->value=$cn->make_array("select ad_id,ad_text 
                        from attr_def 
                        where 
                        not exists (select 1 
                                    from 
                                    attr_min
                                    where 
                                    frd_id=$1 and ad_id=attr_def.ad_id)", NULL,
                        [$this->table->frd_id]);
                echo _("Attribut à ajouter");
                echo $sel_attribut->input();
                $js_script=sprintf("category_card.add_attribut('%s','%s','%s')",
                        $dossier_id, $this->table->frd_id, $objname);
                echo Icon_Action::icon_add(uniqid(), $js_script);
            }
            echo \HtmlInput::hidden("attribut_order", "");
            echo create_script("Sortable.create('{$objname}_list',{
            onUpdate:function(){document.getElementById('attribut_order').value=Sortable.serialize('{$objname}_list')}})");
        }
     
    }

    /**
     * @brief When adding a template of category  of card, the minimum is the name
     * and the quickcode, which must be added into attr_min
     */
    function add_mandatory_attr()
    {
        $cn=Dossier::connect();
        $frd_id=$this->get_table()->getp("frd_id");
        $cn->exec_sql("insert into attr_min (frd_id,ad_id,ad_default_order) values ($1,$2,$3)",
                [$frd_id, ATTR_DEF_NAME,1]);
        $cn->exec_sql("insert into attr_min (frd_id,ad_id,ad_default_order) values ($1,$2,$3)",
                [$frd_id, ATTR_DEF_QUICKCODE,10000]);
    }

    /**
     * @brief save also the order
     * @return void
     */
    function save()
    {
        parent::save();
        $cn=$this->get_table()->get_cn();
        $table_sql=$this->get_table();
        $http=new HttpInput();
        $ctl=$http->request("ctl")."_list";
        $attribut_order=$http->post('attribut_order','string','');
        if ($attribut_order=='') return;
        parse_str($attribut_order,$aOrder);
        if ( isset($aOrder[$ctl])) {
            $order = 10;
            foreach( $aOrder[$ctl] as $item) {
                $ad_id = str_replace('elt','',$item);
                $cn->exec_sql("update attr_min set ad_default_order = $1 where ad_id=$2 and frd_id=$3",
                [$order,$ad_id,$table_sql->get('frd_id')]);
                $order+=10;
            }
        }

    }

}
