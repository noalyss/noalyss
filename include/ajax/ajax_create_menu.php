<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
  /**
   *@file
   *@brief Menu creation
   */

if ( ! defined ('ALLOWED') ) die('Appel direct ne sont pas permis');
$msg=_("Création");
$m=new Menu_Ref($cn);
echo '<form method="POST" id="ajax_create_menu_frm" onsubmit="return confirm_box(this,\''._('Vous confirmez ?').'\')">';
echo HtmlInput::hidden('create_menu', 1);
require_once NOALYSS_TEMPLATE.'/menu_detail.php';
echo HtmlInput::submit('create_menubt',_('Sauver'));
echo HtmlInput::button_close('divmenu');
echo '</form>';
?>
