<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu

/*! \file
 * \brief Contains all the variable + the javascript
 * and some parameter
 */

global $version_noalyss;
// version 
define('NOALYSS_VERSION', 9303 );

// Database schema version 
define("DBVERSION", 203);

// version for MONO_DATABASE
define("MONO_DATABASE", 25);

// Version schema of account_repository database
define("DBVERSIONREPO", 20);
/*
 * Include path
 */
$inc_path = get_include_path();
$dirname = dirname(__FILE__);

/* Global variable of the include dir */
global $g_include_dir, $g_ext_dir, $g_template_dir;
$g_include_dir = $dirname;
$g_ext_dir = $dirname . "/ext";
$g_template_dir = $dirname . "/template";

if (file_exists($dirname . '/config.inc.php')) require_once $dirname . '/config.inc.php';

if (!defined("NOALYSS_HOME")) define("NOALYSS_HOME", dirname($dirname) . "/html");
if (!defined("NOALYSS_BASE")) define("NOALYSS_BASE", dirname($dirname) );
if (!defined("NOALYSS_PLUGIN")) define("NOALYSS_PLUGIN", $g_ext_dir);
if (!defined("NOALYSS_INCLUDE")) define("NOALYSS_INCLUDE", $g_include_dir);
if (!defined("NOALYSS_TEMPLATE")) define("NOALYSS_TEMPLATE", $g_template_dir);
// pdftk can deal with all the PDF , for some of them it is preferable to fix it
// with convert , see also PDF2PS and PS2PDF if yes
if (!defined("FIX_BROKEN_PDF")) define("FIX_BROKEN_PDF", 'NO');

// version < 6.9.1.4 , the default administrator was phpcompta
if (!defined('NOALYSS_ADMINISTRATOR')) {
    define('NOALYSS_ADMINISTRATOR', 'phpcompta');
}
if (!defined("SESSION_KEY")) {
    define("SESSION_KEY", "RtYu0uu");
}
require_once NOALYSS_INCLUDE . '/constant.security.php';

if (strpos($inc_path, ";") != 0) {
    $new_path = $inc_path . ';' . $dirname;
    $os = 0;            /* $os is 0 for windoz */
} else {
    $new_path = $inc_path . ':' . $dirname;
    $os = 1;            /* $os is 1 for unix */
}
set_include_path($new_path);
@ini_set('default_charset', "UTF-8");
@ini_set('session.use_cookies', 1);
// deprecated
@ini_set('magic_quotes_gpc', 'off');

if (!defined('OVERRIDE_PARAM')) {
    ini_set('max_execution_time', 240);
    ini_set('memory_limit', '256M');
}
@ini_set('session.use_trans_sid', 'on');
@session_start();

/*
 * Ini session
 */

if (!defined('NOALYSS_CAPTCHA')) {
    define("NOALYSS_CAPTCHA", false);
}

global $g_failed, $g_succeed;
$g_failed = "<span style=\"font-size:18px;color:red\">x</span>";
$g_succeed = "<span style=\"font-size:18px;color:green\">&#x2713;</span>";
define('SMALLX', '#xe816;');
define('BUTTONADD', "&#10010;");
define('ICON_SEARCH', '&#x1F50E;');
define('ICON_CLEAN', '&#x2715;');

// If noalyss_version is not defined it is likely directly taken from
// git and so this variable is not set, this cause some issue
if (!defined('DEBUGNOALYSS')) {
    define("DEBUGNOALYSS", 0);
}

if (!defined('LOGINPUT')) {
    define("LOGINPUT", false);
}

$version_noalyss = NOALYSS_VERSION;

// If you don't want to be notified of the update
if (!defined("SITE_UPDATE"))
    define("SITE_UPDATE", 'https://www.noalyss.eu/last_version.txt');
if (!defined("SITE_UPDATE_PLUGIN"))
    define("SITE_UPDATE_PLUGIN", 'https://www.noalyss.eu/plugin_last_version.txt');
if (!defined("NOALYSS_PACKAGE_REPOSITORY")) {
    define("NOALYSS_PACKAGE_REPOSITORY", "https://package.noalyss.eu/");
}
// If you don't want that the system information  is accessible
if (!defined("SYSINFO_DISPLAY")) {
    define("SYSINFO_DISPLAY", TRUE);
}
define('NOTFOUND', '--not found--');
define("MAX_COMPTE", 4);
define('MAX_ARTICLE', 5);
define('MAX_ARTICLE_STOCK', 10);
define('MAX_CAT', 15);
define('MAX_CARD_SEARCH', 550);
define('MAX_FORECAST_ITEM', 10);
define('MAX_PREDEFINED_OPERATION', 50);
define('MAX_COMPTE_CARD', 4);
define('COMPTA_MAX_YEAR', 2100);
define('COMPTA_MIN_YEAR', 1900);
define('MAX_RECONCILE', 25);
define('MAX_QCODE', 4);
if (!defined('MAX_SEARCH_CARD')) {
    define('MAX_SEARCH_CARD', 20);
}

define('MAX_FOLDER_TO_SHOW', 80);
define('MAX_ACTION_SHOW', 20);

if (DEBUGNOALYSS == 0) {
    // PRODUCTION : nothing is displaid , report only errors and warning
    // Rapporte les erreurs d'exécution de script
    error_reporting(E_ERROR | E_WARNING);
    ini_set("display_errors", 0);
    ini_set("html_errors", 0);
    ini_set('log_errors', 1);
    ini_set('log_errors_max_len', 0);
    if(function_exists('xdebug_disable')) { xdebug_disable(); }
} elseif (DEBUGNOALYSS == 1) {
    /* DEVELOPPEMENT : display all errors warning notice deprecated ...*/
    error_reporting(2147483647);
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);
    ini_set("html_errors", 1);
    ini_set('log_errors', 1);
    ini_set('log_errors_max_len', 0);
} elseif (DEBUGNOALYSS == 2) {
    // like level 1 plus extra  info (filename, ...)
    error_reporting(2147483647);
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);
    ini_set("html_errors", 1);
    ini_set('log_errors', 1);
    ini_set('log_errors_max_len', 0);


}
// Erreur
define("NOERROR", 0);
define("BADPARM", 1);
define("BADDATE", 2);
define("NOTPERIODE", 3);
define("PERIODCLOSED", 4);
define("INVALID_ECH", 5);
define("RAPPT_ALREADY_USED", 6);
define("RAPPT_NOT_EXIST", 7);
define("DIFF_AMOUNT", 8);
define("RAPPT_NOMATCH_AMOUNT", 9);
define("NO_PERIOD_SELECTED", 10);
define("NO_POST_SELECTED", 11);
define("LAST", 1);
define("FIRST", 0);
define("ERROR", 12);

//!<ACTION  defines document_type for action
define('ACTION', '1,5,6,7,8');

//valeurs standardd
define("YES", 1);
define("NO", 0);
define("OPEN", 1);
define("CLOSED", 0);
define("NOTCENTRALIZED", 3);
define("ALL", 4);

// Pour les ShowMenuComptaLeft
define("MENU_FACT", 1);
define("MENU_FICHE", 2);
define("MENU_PARAM", 3);

// for the fiche_inc.GetSqlFiche function
define("ALL_FICHE_DEF_REF", 1000);

// fixed value for attr_def data
define("ATTR_DEF_ACCOUNT", 5);
define("ATTR_DEF_NAME", 1);
define("ATTR_DEF_BQ_NO", 3);
define("ATTR_DEF_BQ_NAME", 4);
define("ATTR_DEF_PRIX_ACHAT", 7);
define("ATTR_DEF_PRIX_VENTE", 6);
define("ATTR_DEF_TVA", 2);
define("ATTR_DEF_NUMTVA", 13);
define("ATTR_DEF_ADRESS", 14);
define("ATTR_DEF_CP", 15);
define("ATTR_DEF_PAYS", 16);
define("ATTR_DEF_STOCK", 19);
define("ATTR_DEF_TEL", 17);
define("ATTR_DEF_EMAIL", 18);
define("ATTR_DEF_CITY", 24);
define("ATTR_DEF_COMPANY", 25);
define("ATTR_DEF_FAX", 26);
define("ATTR_DEF_NUMBER_CUSTOMER", 30);
define("ATTR_DEF_DEP_PRIV", 31);
define("ATTR_DEF_DEPENSE_NON_DEDUCTIBLE", 20);
define("ATTR_DEF_TVA_NON_DEDUCTIBLE", 21);
define("ATTR_DEF_TVA_NON_DEDUCTIBLE_RECUP", 22);
define("ATTR_DEF_QUICKCODE", 23);
define("ATTR_DEF_FIRST_NAME", 32);
define("ATTR_DEF_WEBSITE", 34);

define('ATTR_DEF_ACCOUNT_ND_TVA', 50);
define('ATTR_DEF_ACCOUNT_ND_TVA_ND', 51);
define('ATTR_DEF_ACCOUNT_ND_PERSO', 52);
define('ATTR_DEF_ACCOUNT_ND', 53);
define('ATTR_DEF_ACTIF', 54);

define("FICHE_TYPE_CLIENT", 9);
define("FICHE_TYPE_VENTE", 1);
define("FICHE_TYPE_FOURNISSEUR", 8);
define("FICHE_TYPE_FIN", 4);
define("FICHE_TYPE_CONTACT", 16);
define("FICHE_TYPE_EMPL", 25);
define("FICHE_TYPE_ADM_TAX", 14);
define("FICHE_TYPE_ACH_MAR", 2);
define("FICHE_TYPE_ACH_SER", 3);
define("FICHE_TYPE_ACH_MAT", 7);
define("FICHE_TYPE_PROJET", 26);
define("FICHE_TYPE_MATERIAL", 7);
// Max size is defined by default to 2MB,
if (!defined("MAX_FILE_SIZE")) {
    define("MAX_FILE_SIZE", 2097152);
}
/**
 * -- pour utiliser unoconv démarrer un server libreoffice
 * commande
 * libreoffice --headless --accept="socket,host=127.0.0.1,port=2002;urp;" --nofirststartwizard
 * ou
 *  unoconv -l -v -s localhost
 */
if (!defined('OFFICE')) define('OFFICE', '');
if (!defined('GENERATE_PDF')) define('GENERATE_PDF', 'NO');

/**
 * Pour conversion GIF en PDF
 */
$convert_gif_pdf = '/usr/bin/convert';
if (file_exists($convert_gif_pdf)) {
    define('CONVERT_GIF_PDF', $convert_gif_pdf);
} else {
    define('CONVERT_GIF_PDF', 'NOT');

}
/**
 * PDF2PS is used when the PDF is broken , used with FIX_BROKEN_PDF
 */
$pdf2ps = '/usr/bin/pdf2ps';

if (!file_exists($pdf2ps))
    define('PDF2PS', 'NOT');
else
    define('PDF2PS', $pdf2ps);
/**
 * PS2PDF is used when the PDF is broken , used with FIX_BROKEN_PDF
 */
$ps2pdf = '/usr/bin/ps2pdf';

if (!file_exists($ps2pdf))
    define('PS2PDF', 'NOT');
else
    define('PS2PDF', $ps2pdf);


/**
 * Outil pour manipuler les PDF
 */
if (!isset ($pdftk)) {
    $pdftk = '/usr/bin/pdftk';
}
if (file_exists($pdftk)) {
    define('PDFTK', $pdftk);
} else {
    define('PDFTK', 'NOT');
}

// If it is not a mono folder it is a multi one
if (!defined('MULTI')) {
    define('MULTI', 1);
}

define('JS_INFOBULLE', '
        <DIV id="bulle" class="infobulle"></DIV>
        <script type="text/javascript" language="javascript"  src="js/infobulle.js">
        </script>');


// Sql string
define("SQL_LIST_ALL_INVOICE", "");

define("SQL_LIST_UNPAID_INVOICE", "  (jr_rapt is null or jr_rapt = '') and jr_valid = true  "
);


define("SQL_LIST_UNPAID_INVOICE_DATE_LIMIT", "
        where (jr_rapt is null or jr_rapt = '')
        and to_date(to_char(jr_ech,'DD.MM.YYYY'),'DD.MM.YYYY') < to_date(to_char(now(),'DD.MM.YYYY'),'DD.MM.YYYY')
        and jr_valid = true");

/**
 * Exception
 */
// Limit email exceeds parameter
define('EMAIL_LIMIT', 1002);
define('EXC_PARAM_VALUE', 1005);
define('EXC_PARAM_TYPE', 1006);
define('EXC_DUPLICATE', 1200);
define('EXC_INVALID', 1400);
define('EXC_FORBIDDEN', 1500);
// exception when balance is incorrect when saving an operation
define('EXC_BALANCE', 1501);
define("UNPINDG", "&#xf047;");
define("PINDG", "&#xe809;");

// Url of NOALYSS (http://...) 
// 
if (!defined("NOALYSS_URL")) {
    $protocol = "http";
    if (isset ($_SERVER['REQUEST_SCHEME'])) {
        $protocol = $_SERVER['REQUEST_SCHEME'];
    }
    $base = $protocol . '://' .
        $_SERVER['SERVER_NAME'] .
        ":" . $_SERVER['SERVER_PORT'] .
        dirname($_SERVER['PHP_SELF']);
    define("NOALYSS_URL", $base);
}
if (!defined("DEFAULT_SERVER_VIDEO_CONF")) {
    define("DEFAULT_SERVER_VIDEO_CONF", "https://www.free-solutions.org/");
}

define ("VATCHECK_URL","https://ec.europa.eu/taxation_customs/vies/rest-api/");

/**
 * @brief load automatically class
 *
 * @param string $class classname to load
 */
function noalyss_class_autoloader($class)
{
    $class = strtolower($class);

    foreach (array("class","lib","database") as $path) {
        if ( file_exists(NOALYSS_INCLUDE.'/'.$path.'/'.$class.'.class.php')) {
            require_once  NOALYSS_INCLUDE.'/'.$path.'/'.$class.'.class.php';
            return;
        }

    }
    $aClass = array(
        "database" => "class/database.class.php",
        "acc_detail" => "class/acc_operation.class.php",
        "acc_sold" => "class/acc_operation.class.php",
        "acc_misc" => "class/acc_operation.class.php",
        "acc_purchase" => "class/acc_operation.class.php",
        "acc_fin" => "class/acc_operation.class.php",
        "lettering_card" => "class/lettering.class.php",
        "lettering_account" => "class/lettering.class.php",
        "print_ledger_financial" => "class/print_ledger_fin.class.php",
        "anc_key_ledger_sql" => "database/anc_key_sql.class.php",
        "anc_key_detail_sql" => "database/anc_key_sql.class.php",
        "anc_key_activity_sql" => "database/anc_key_sql.class.php",
        "databasecore" => "lib/database_core.class.php",
        "httpinput" => "lib/http_input.class.php",
        "ismallbutton" => "lib/ibutton.class.php",
        "inputswitch" => "lib/input_switch.class.php",
        'noalyss\mobile' => "class/mobile.class.php",
        "htmlinput" => "lib/html_input.class.php",
        'noalyss\dbg'=>"lib/dbg.php",
        'noalyss\file_cache'=>"lib/file_cache.class.php",
        "pdfland"=>"class/pdf_land.class.php",
        "noalyss\widget\widget"=>"widget/widget.php"
    );
    if (isset ($aClass[$class])) {
        require_once NOALYSS_INCLUDE . "/" . $aClass[$class];
    }

}

spl_autoload_register('\noalyss_class_autoloader', true);
