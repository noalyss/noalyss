<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Copyright Author Dany De Bontridder danydb@aevalys.eu


/**
 * @file 
 * @brief deprecated Noalyss_SQL class 
 * 
 */

/**
 * @class Noalyss_SQL
 * @brief this class is deprecated and inherits from Table_Data_SQL. It remains only for compatibility
 * purpose
 * @see Table_Data_SQL
 * @deprecated since version 9.0
  */
class Noalyss_SQL extends Table_Data_SQL
{
    
}