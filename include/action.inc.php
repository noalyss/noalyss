<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu
/*! \file
 * \brief Page who manage the different actions (meeting, letter)
 */
if ( ! defined ('ALLOWED') ) die('Appel direct ne sont pas permis');
global $g_user;
$http=new HttpInput();

$retour=HtmlInput::button_anchor(_('Retour liste'),
	HtmlInput::request_to_string(array("closed_action","remind_date_end","remind_date","sag_ref","only_internal",
            "state","ac","gDossier","qcode","ag_dest_query","action_query","tdoc","date_start","date_end","hsstate",
            "searchtag",
            "closed_action",
            "tag_option",)),
        "","","smallbutton");
//-----------------------------------------------------
// Follow_Up
//-----------------------------------------------------
/*!\brief Show the list of action, this code should be common
 *        to several webpage. But for the moment we keep like that
 *        because it is used only by this file.
 *\param $cn database connection
 * \param $retour button for going back
 * \param $h_url calling url
 */

// We need a sub action (3rd level)
// show a list of already taken action
// propose to add one
// permit also a search
// show detail
$sub_action=$http->request("sa","string","");
/* if ag_id is set then we give it otherwise we have problem
 * with the generation of document
 */
$ag_id=$http->request("ag_id","string","0");

$base="do.php?".http_build_query(["ac"=>$http->request("ac"),"gDossier"=>Dossier::id()]);
echo '<div class="content">';
require_once NOALYSS_INCLUDE.'/action.common.inc.php';
echo "</div>";

?>
