<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright Author Dany De Bontridder danydb@aevalys.eu
/*! 
 * \file
 * \brief Send a ledger in CSV format , receives 
 *    - jrn_id id of the ledger
 *    - p_simple L list , D detailled, A accounting, E extended
 *    - from periode
 *    - to periode
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

include_once NOALYSS_INCLUDE."/lib/ac_common.php";

global $cn,$g_user;


$gDossier=dossier::id();
$http=new HttpInput();

$export=new Noalyss_Csv(_('journal'));

$export->send_header();


/*
 * Variable from $_GET
 */
try
{
    $get_jrn=$http->get('jrn_id', "number");
    $get_option=$http->get('p_simple', "string");
    $get_from_periode=$http->get('from_periode', 'number');
    $get_to_periode=$http->get('to_periode', 'number');
    $filter_operation=$http->get("operation_type");
}
catch (Exception $exc)
{
    echo $exc->getMessage();
    error_log($exc->getTraceAsString());
    throw $exc;
}

$g_user->Check();
$g_user->check_dossier($gDossier);

//----------------------------------------------------------------------------
// $get_jrn == 0 when request for all ledger, in that case, we must filter
// the legder with the security in Acc_Ledger::get_row
//----------------------------------------------------------------------------
if ($get_jrn!=0&&$g_user->check_jrn($get_jrn)=='X')
{
    NoAccess();
    exit();
}
global $g_user;
/**
 * for all ledgers
 */
if ($get_jrn==0)
{
    // find out all the available ledgers for the current user
    $a_jrn=Print_Ledger::available_ledger($get_from_periode);
    $Jrn=new Acc_Ledger($cn,0);
}
else
{
    $a_jrn=$get_jrn;
    $Jrn=new Acc_Ledger($cn, $get_jrn);
}

$Jrn->get_name();
$jrn_type=$Jrn->get_type();

//
// With Detail per item which is possible only for VEN or ACH
//  For Detailled VAT for ACH or VEN
//  ODS or all ledgers becomes A
//  Extended but no FIN becomes L
// 
if ( $get_option=="E")
{
    if ($jrn_type=='FIN')
    {
        $get_option='A';
    }
    elseif ($jrn_type=='ODS'||$Jrn->id==0)
    {
        $get_option='A';
    }
    else
    {
        switch ($jrn_type)
        {
            case 'VEN':
                $ledger=new Acc_Ledger_Sale($cn, $get_jrn);
                $ret_detail=$ledger->get_detail_sale($get_from_periode,
                        $get_to_periode,$filter_operation);
                $a_heading=Acc_Ledger_Sale::heading_detail_sale();

                break;
            case 'ACH':
                $ledger=new Acc_Ledger_Purchase($cn, $get_jrn);
                $ret_detail=$ledger->get_detail_purchase($get_from_periode,
                        $get_to_periode,$filter_operation);
                $a_heading=Acc_Ledger_Purchase::heading_detail_purchase();
                break;
            default:
                die(__FILE__.":".__LINE__.'Journal invalide');
                break;
        }
        if ($ret_detail==null)
            return;

        $a_heading[]=_("Date paiement");
        $a_heading[]=_("Montant paiement");
        $a_heading[]=_("Methode paiement");
        $a_heading[]=_("Libellé");
        $a_heading[]=_("Opération paiement");
        
        // Prepare the query for reconcile date
        $prepared_query=new Prepared_Query($cn);
        $prepared_query->prepare_reconcile_date();
        
        $nb=Database::num_row($ret_detail);
        $title=array();
        foreach ($a_heading as $key=> $value)
        {
            $title[]=$value;
        }
        for ($i=0; $i<$nb; $i++)
        {
            $row=Database::fetch_array($ret_detail, $i);
            if ($i==0)
            {
                $export->write_header($title);
            }
            $a_row=array();
            $type="text";
            $idx_col=0;
            foreach ($row as $col)
            {
                if ($idx_col> 18 ) $type="number";
                $export->add($col,$type);
                $idx_col++;
            }
            //info payment
            $ret_reconcile=$cn->execute('reconcile_date',array($row['jr_id']));
            $max=Database::num_row($ret_reconcile);
            for ($e=0;$e<$max;$e++) {
                $row=Database::fetch_array($ret_reconcile, $e);
                $export->add($row['jr_date']);
                $export->add($row['jr_montant'],"number");
                $export->add($row['qcode_bank']);
                $export->add($row['qcode_name']);
                $export->add($row['jr_internal']);
            }
            $export->write();
        }
    }
}
//-----------------------------------------------------------------------------
// Detailled printing
// For miscellaneous legder or all ledgers
//-----------------------------------------------------------------------------
if ($get_option=='A')
{
    if ($get_jrn == 0 )
    {
        $acc_ledger_history=new Acc_Ledger_History_Generic($cn, $a_jrn,
            $get_from_periode, $get_to_periode, 'A');
    } else {
        $acc_ledger_history=new Acc_Ledger_History_Generic($cn, array($a_jrn),
            $get_from_periode, $get_to_periode, 'A');
        
    }
     $acc_ledger_history->set_filter_operation($filter_operation);
    $acc_ledger_history->export_csv();
    exit;
}
//-----------------------------------------------------------------------------
//  Mode list for ODS , FIN and GL
//-----------------------------------------------------------------------------
if ($get_option=="L" && ($jrn_type=='ODS'||$jrn_type=='FIN'||$jrn_type=='GL') )
{
    if ( $get_jrn==0) {
        $Row=$Jrn->get_rowSimple($get_from_periode, $get_to_periode, $a_jrn);
    }else {
        $Row=$Jrn->get_rowSimple($get_from_periode, $get_to_periode);
    }
     // Prepare the query for reconcile date
    $prepared_query=new Prepared_Query($cn);
    $prepared_query->prepare_reconcile_date();
    
    $title=array();
    $title[]=_("operation");
    $title[]=_("Date");
    $title[]=_("N° Pièce");
    $title[]=_("QuickCode");
    $title[]=_("Tiers");
    $title[]=_("commentaire");
    $title[]=_("internal");
    $title[]=_("montant");
    $title[]=_("montant devise");
    $title[]=_("devise");
    $title[]=_("taux");
    $title[]=_("taux réf");
    $export->write_header($title);
    foreach ($Row as $line)
    {
        $tiers_id=$Jrn->get_tiers_id($line['jrn_def_type'], $line['jr_id']);
        $fiche_tiers=new Fiche($cn, $tiers_id);
        $tiers=$fiche_tiers->strAttribut(ATTR_DEF_NAME, 0)." ".$fiche_tiers->strAttribut(ATTR_DEF_FIRST_NAME,
                        0);

        $export->add($line['num']);
        $export->add($line['date']);
        $export->add($line['jr_pj_number']);
        $export->add($fiche_tiers->get_quick_code());
        $export->add($tiers);
        $export->add($line['comment']);
        $export->add($line['jr_internal']);
        //	  echo "<TD>".$line['pj'].";";
        // If the ledger is financial :
        // the credit must be negative and written in red
        // Get the jrn type
        if ($line['jrn_def_type']=='FIN')
        {
            $positive=$cn->get_value("select qf_amount from quant_fin  ".
                    " where jr_id=$1", array($line['jr_id']));

            $export->add($positive, "number");
            // for financial , all the rows are in the table operation_currency and then the
            // amount is doubled
            $export->add(bcdiv($line['sum_ocamount'],2,4),"number");
        }
        else
        {
            $export->add($line['montant'], "number");
            $export->add(bcadd($line['sum_ocamount'],$line['sum_ocvat_amount']),"number");
        }
        //-- add currency
       $export->add($line['cr_code_iso']);
       $export->add($line['currency_rate']);
       $export->add($line['currency_rate_ref']);
       
        //------ Add reconcilied operation ---------------
        $ret_reconcile=$cn->execute('reconcile_date',
                array($line['jr_id']));
        $max=Database::num_row($ret_reconcile);
        if ($max>0)
        {
            for ($e=0; $e<$max; $e++)
            {
                $row=Database::fetch_array($ret_reconcile, $e);
                $export->add($row['qcode_bank']);
                $export->add($row['qcode_name']);
                $export->add($row['jr_date']);
                $export->add($row['jr_internal']);
                $export->add($row['jr_pj_number']);
                 $export->add($row['jr_montant'],"number");
            }
        }
        $export->write();
    }
    return;
}

//-----------------------------------------------------------------------------
// Detail printing for ACH or VEN : 1 row resume the situation with VAT, DNA
// for Misc the amount 
// For Financial only the tiers and the sign of the amount
//-----------------------------------------------------------------------------
if ($get_option=="L" || $get_option == 'D')
{

//-----------------------------------------------------
    if ($jrn_type=='ODS'||$jrn_type=='FIN'||$jrn_type=='GL') 
    {
         if ($get_jrn == 0 )
        {
            $acc_ledger_history=new Acc_Ledger_History_Generic($cn, $a_jrn,
                $get_from_periode, $get_to_periode, 'D');
        } else {
            $acc_ledger_history=new Acc_Ledger_History_Generic($cn, array($a_jrn),
                $get_from_periode, $get_to_periode, 'D');

        }
        $acc_ledger_history->set_filter_operation($filter_operation);
        $acc_ledger_history->export_csv();
        return;
    }
//------------------------------------------------------------------------------
// One line summary with tiers, amount VAT, DNA, tva code ....
// 
//------------------------------------------------------------------------------
    if ($jrn_type=="ACH")
    {
        $acc_ledger_history=new Acc_Ledger_History_Purchase($cn, [$Jrn->id],
                $get_from_periode, $get_to_periode, 'D');
        $acc_ledger_history->set_filter_operation($filter_operation);
        $acc_ledger_history->export_csv();
    }
    if ($jrn_type=="VEN")
    {
        $acc_ledger_history=new Acc_Ledger_History_Sale($cn, [$Jrn->id],
                $get_from_periode, $get_to_periode, 'D');
        $acc_ledger_history->set_filter_operation($filter_operation);
        $acc_ledger_history->export_csv();
    }
}
?>
