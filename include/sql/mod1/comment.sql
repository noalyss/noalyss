COMMENT ON FUNCTION comptaproc.correct_sequence(p_sequence text, p_col text, p_table text) IS ' Often the primary key is a sequence number and sometimes the value of the sequence is not synchronized with the primary key ( p_sequence : sequence name, p_col : col of the pk,p_table : concerned table';
COMMENT ON FUNCTION comptaproc.format_account(p_account public.account_type) IS 'format the accounting :
- upper case
- remove space and special char.
';
COMMENT ON FUNCTION comptaproc.format_quickcode(p_qcode text) IS 'Put in upper case and remove invalid char';
COMMENT ON FUNCTION comptaproc.four_upper_letter() IS 'Cut to the 4 first letter in uppercase';
COMMENT ON FUNCTION comptaproc.html_quote(p_string text) IS 'remove harmfull HTML char';
COMMENT ON FUNCTION comptaproc.update_quick_code(njft_id integer, tav_text text) IS 'update the qcode + related columns in other cards';
COMMENT ON TABLE public.acc_other_tax IS 'Additional tax for Sale or Purchase ';
COMMENT ON COLUMN public.acc_other_tax.ac_label IS 'Label of the tax';
COMMENT ON COLUMN public.acc_other_tax.ac_rate IS 'rate of the tax in percent';
COMMENT ON COLUMN public.acc_other_tax.ajrn_def_id IS 'array of to FK jrn_def (jrn_def_id)';
COMMENT ON COLUMN public.acc_other_tax.ac_accounting IS 'FK tmp_pcmn (pcm_val)';
COMMENT ON TABLE public.action IS 'The different privileges';
COMMENT ON COLUMN public.action.ac_code IS 'this code will be used in the code with the function User::check_action ';
COMMENT ON TABLE public.action_detail IS 'Detail of action_gestion, see class Action_Detail';
COMMENT ON COLUMN public.action_detail.f_id IS 'the concerned	card';
COMMENT ON COLUMN public.action_detail.ad_text IS ' Description ';
COMMENT ON COLUMN public.action_detail.ad_pu IS ' price per unit ';
COMMENT ON COLUMN public.action_detail.ad_quant IS 'quantity ';
COMMENT ON COLUMN public.action_detail.ad_tva_id IS ' tva_id ';
COMMENT ON COLUMN public.action_detail.ad_tva_amount IS ' tva_amount ';
COMMENT ON COLUMN public.action_detail.ad_total_amount IS ' total amount';
COMMENT ON TABLE public.action_gestion IS 'Contains the details for the follow-up of customer, supplier, administration';
COMMENT ON COLUMN public.action_gestion.ag_type IS ' type of action: see document_type ';
COMMENT ON COLUMN public.action_gestion.f_id_dest IS 'third party';
COMMENT ON COLUMN public.action_gestion.ag_title IS 'title';
COMMENT ON COLUMN public.action_gestion.ag_ref IS 'its reference';
COMMENT ON COLUMN public.action_gestion.ag_priority IS 'Low, medium, important';
COMMENT ON COLUMN public.action_gestion.ag_dest IS 'is the profile which has to take care of this action';
COMMENT ON COLUMN public.action_gestion.ag_owner IS 'is the owner of this action';
COMMENT ON COLUMN public.action_gestion.ag_contact IS 'contact of the third part';
COMMENT ON COLUMN public.action_gestion.ag_state IS 'state of the action same as document_state';
COMMENT ON TABLE public.action_gestion_comment IS 'comment on action management';
COMMENT ON COLUMN public.action_gestion_comment.agc_id IS 'PK';
COMMENT ON COLUMN public.action_gestion_comment.ag_id IS 'FK to action_gestion';
COMMENT ON COLUMN public.action_gestion_comment.agc_comment IS 'comment';
COMMENT ON COLUMN public.action_gestion_comment.tech_user IS 'user_login';
COMMENT ON TABLE public.action_gestion_operation IS 'Operation linked on action';
COMMENT ON COLUMN public.action_gestion_operation.ago_id IS 'pk';
COMMENT ON COLUMN public.action_gestion_operation.ag_id IS 'fk to action_gestion';
COMMENT ON COLUMN public.action_gestion_operation.jr_id IS 'fk to jrn';
COMMENT ON TABLE public.action_gestion_related IS 'link between action';
COMMENT ON COLUMN public.action_gestion_related.aga_id IS 'pk';
COMMENT ON COLUMN public.action_gestion_related.aga_least IS 'fk to action_gestion, smallest ag_id';
COMMENT ON COLUMN public.action_gestion_related.aga_greatest IS 'fk to action_gestion greatest ag_id';
COMMENT ON COLUMN public.action_gestion_related.aga_type IS 'Type de liens';
COMMENT ON TABLE public.action_person IS 'Person involved in the action';
COMMENT ON COLUMN public.action_person.ap_id IS 'pk';
COMMENT ON COLUMN public.action_person.ag_id IS 'fk to fiche';
COMMENT ON TABLE public.action_person_option IS 'option for each contact';
COMMENT ON COLUMN public.action_person_option.ap_value IS 'Value of the option';
COMMENT ON COLUMN public.action_person_option.contact_option_ref_id IS 'FK to contact_option';
COMMENT ON COLUMN public.action_person_option.action_person_id IS 'FK to action_person';
COMMENT ON TABLE public.action_tags IS 'Tags link to  action';
COMMENT ON TABLE public.attr_def IS 'The available attributs for the cards';
COMMENT ON COLUMN public.attr_def.ad_search_followup IS '1 : search  available  from followup , 0  : search not available in followup';
COMMENT ON COLUMN public.attr_def.ad_default_order IS 'Default order of the attribute';
COMMENT ON TABLE public.attr_min IS 'The value of  attributs for the cards';
COMMENT ON TABLE public.bilan IS 'contains the template and the data for generating different documents  ';
COMMENT ON COLUMN public.bilan.b_id IS 'primary key';
COMMENT ON COLUMN public.bilan.b_name IS 'Name of the document';
COMMENT ON COLUMN public.bilan.b_file_template IS 'path of the template (document/...)';
COMMENT ON COLUMN public.bilan.b_file_form IS 'path of the file with forms';
COMMENT ON COLUMN public.bilan.b_type IS 'type = ODS, RTF...';
COMMENT ON TABLE public.bookmark IS 'Bookmark of the connected user';
COMMENT ON TABLE public.centralized IS 'The centralized journal';
COMMENT ON TABLE public.contact_option_ref IS 'Option for the contact';
COMMENT ON COLUMN public.contact_option_ref.cor_label IS 'Label de l''option';
COMMENT ON COLUMN public.contact_option_ref.cor_type IS '0 text , 1 select ,2 nombre , 3 date';
COMMENT ON COLUMN public.contact_option_ref.cor_value_select IS 'Select values';
COMMENT ON TABLE public.currency IS 'currency';
COMMENT ON COLUMN public.currency.cr_code_iso IS 'Code ISO';
COMMENT ON COLUMN public.currency.cr_name IS 'Name of the currency';
COMMENT ON TABLE public.currency_history IS 'currency values history';
COMMENT ON COLUMN public.currency_history.id IS 'pk';
COMMENT ON COLUMN public.currency_history.ch_value IS 'rate of currency depending of currency of the folder';
COMMENT ON COLUMN public.currency_history.ch_from IS 'Date when the rate is available';
COMMENT ON COLUMN public.currency_history.currency_id IS 'FK to currency';
COMMENT ON TABLE public.del_action IS 'deleted actions';
COMMENT ON TABLE public.del_jrn IS 'deleted operation';
COMMENT ON TABLE public.del_jrnx IS 'delete operation details';
COMMENT ON TABLE public.document IS 'This table contains all the documents : summary and lob files';
COMMENT ON TABLE public.document_component IS 'Give the component of NOALYSS that is using is';
COMMENT ON COLUMN public.document_component.dc_id IS 'PK';
COMMENT ON COLUMN public.document_component.dc_code IS 'Code used in document_modele';
COMMENT ON COLUMN public.document_component.dc_comment IS 'Code used in document_modele';
COMMENT ON TABLE public.document_modele IS ' contains all the template for the  documents';
COMMENT ON TABLE public.document_option IS 'Reference of option addable to document_type';
COMMENT ON COLUMN public.document_option.do_code IS 'Code of the option to add';
COMMENT ON COLUMN public.document_option.document_type_id IS 'FK to document_type';
COMMENT ON COLUMN public.document_option.do_enable IS '1 the option is activated, 0 is inativated';
COMMENT ON COLUMN public.document_option.do_option IS 'Option for the detail';
COMMENT ON SEQUENCE public.document_seq IS 'Sequence for the sequence bound to the document modele';
COMMENT ON TABLE public.document_state IS 'State of the document';
COMMENT ON TABLE public.document_type IS 'Type of document : meeting, invoice,...';
COMMENT ON COLUMN public.document_type.dt_prefix IS 'Prefix for ag_ref';
COMMENT ON TABLE public.extension IS 'Content the needed information for the extension';
COMMENT ON COLUMN public.extension.ex_id IS 'Primary key';
COMMENT ON COLUMN public.extension.ex_name IS 'code of the extension ';
COMMENT ON COLUMN public.extension.ex_code IS 'code of the extension ';
COMMENT ON COLUMN public.extension.ex_desc IS 'Description of the extension ';
COMMENT ON COLUMN public.extension.ex_file IS 'path to the extension to include';
COMMENT ON COLUMN public.extension.ex_enable IS 'Y : enabled N : disabled ';
COMMENT ON TABLE public.fiche IS 'Cards';
COMMENT ON COLUMN public.fiche.f_enable IS 'value = 1 if card enable , otherwise 0 ';
COMMENT ON TABLE public.fiche_def IS 'Cards definition';
COMMENT ON TABLE public.fiche_def_ref IS 'Family Cards definition';
COMMENT ON TABLE public.fiche_detail IS 'join between the card and the attribut definition';
COMMENT ON TABLE public.forecast IS 'contains the name of the forecast';
COMMENT ON TABLE public.forecast_category IS 'Category of forecast';
COMMENT ON COLUMN public.forecast_category.fc_id IS 'primary key';
COMMENT ON COLUMN public.forecast_category.fc_desc IS 'text of the category';
COMMENT ON COLUMN public.forecast_category.f_id IS 'Foreign key, it is the parent from the table forecast';
COMMENT ON COLUMN public.forecast_category.fc_order IS 'Order of the category, used when displaid';
COMMENT ON TABLE public.forecast_item IS 'items of forecast';
COMMENT ON COLUMN public.forecast_item.fi_id IS 'Primary key';
COMMENT ON COLUMN public.forecast_item.fi_text IS 'Label of the i	tem';
COMMENT ON COLUMN public.forecast_item.fi_account IS 'Accountancy entry';
COMMENT ON COLUMN public.forecast_item.fi_order IS 'Order of showing (not used)';
COMMENT ON COLUMN public.forecast_item.fi_amount IS 'Amount';
COMMENT ON COLUMN public.forecast_item.fi_pid IS '0 for every month, or the value parm_periode.p_id ';
COMMENT ON TABLE public.form_definition IS 'Simple Report name';
COMMENT ON TABLE public.form_detail IS 'Simple report details with formula';
COMMENT ON TABLE public.groupe_analytique IS 'Group of analytic accountancy';
COMMENT ON TABLE public.info_def IS 'Contains the types of additionnal info we can add to a operation';
COMMENT ON TABLE public.jnt_document_option_contact IS 'Many to many table between document and contact option';
COMMENT ON TABLE public.jnt_fic_attr IS 'join between the family card and the attribut definition';
COMMENT ON TABLE public.jnt_letter IS 'm2m tables for lettering';
COMMENT ON TABLE public.jnt_tag_group_tag IS 'Many to Many table betwwen tag and tag group';
COMMENT ON TABLE public.jrn IS 'Journal: content one line for a group of accountancy writing';
COMMENT ON COLUMN public.jrn.jr_optype IS 'Type of operation , NOR = NORMAL , OPE opening , EXT extourne, CLO closing';
COMMENT ON TABLE public.jrn_def IS 'Definition of a journal, his properties';
COMMENT ON COLUMN public.jrn_def.jrn_enable IS 'Set to 1 if the ledger is enable ';
COMMENT ON COLUMN public.jrn_def.currency_id IS 'Default currency for financial ledger';
COMMENT ON COLUMN public.jrn_def.jrn_def_negative_amount IS '1 echo a warning if you are not using an negative amount, default 0 for no warning';
COMMENT ON COLUMN public.jrn_def.jrn_def_negative_warning IS 'Yell a warning if the amount if not negative , in the case of jrn_def_negative_amount is Y';
COMMENT ON COLUMN public.jrn_def.jrn_def_quantity IS 'Use the quantity column, 0->disable,1->enable,used only with Sale and Purchase otherwise ignored';
COMMENT ON TABLE public.jrn_info IS 'extra info for operation';
COMMENT ON TABLE public.jrn_note IS 'Note about operation';
COMMENT ON TABLE public.jrn_periode IS 'Period by ledger';
COMMENT ON TABLE public.jrn_rapt IS 'Rapprochement between operation';
COMMENT ON COLUMN public.jrn_tax.j_id IS 'fk jrnx';
COMMENT ON COLUMN public.jrn_tax.pcm_val IS 'FK tmp_pcmn';
COMMENT ON COLUMN public.jrn_tax.ac_id IS 'FK to acc_other_tax';
COMMENT ON TABLE public.jrn_type IS 'Type of journal (Sell, Buy, Financial...)';
COMMENT ON TABLE public.jrnx IS 'Journal: content one line for each accountancy writing';
COMMENT ON TABLE public.key_distribution IS 'Distribution key for analytic';
COMMENT ON COLUMN public.key_distribution.kd_id IS 'PK';
COMMENT ON COLUMN public.key_distribution.kd_name IS 'Name of the key';
COMMENT ON COLUMN public.key_distribution.kd_description IS 'Description of the key';
COMMENT ON TABLE public.key_distribution_activity IS 'Contains the analytic account';
COMMENT ON COLUMN public.key_distribution_activity.ka_id IS 'pk';
COMMENT ON COLUMN public.key_distribution_activity.ke_id IS 'fk to key_distribution_detail';
COMMENT ON COLUMN public.key_distribution_activity.po_id IS 'fk to poste_analytique';
COMMENT ON COLUMN public.key_distribution_activity.pa_id IS 'fk to plan_analytique';
COMMENT ON TABLE public.key_distribution_detail IS 'Row of activity and percent';
COMMENT ON COLUMN public.key_distribution_detail.ke_id IS 'pk';
COMMENT ON COLUMN public.key_distribution_detail.kd_id IS 'fk to key_distribution';
COMMENT ON COLUMN public.key_distribution_detail.ke_row IS 'group order';
COMMENT ON TABLE public.key_distribution_ledger IS 'Legder where the distribution key can be used';
COMMENT ON COLUMN public.key_distribution_ledger.kl_id IS 'pk';
COMMENT ON COLUMN public.key_distribution_ledger.kd_id IS 'fk to key_distribution';
COMMENT ON COLUMN public.key_distribution_ledger.jrn_def_id IS 'fk to jrnd_def, ledger where this key is available';
COMMENT ON TABLE public.letter_cred IS 'Letter cred';
COMMENT ON TABLE public.letter_deb IS 'letter deb';
COMMENT ON TABLE public.menu_default IS 'default menu for certains actions';
COMMENT ON TABLE public.menu_ref IS 'Definition of all the menu';
COMMENT ON COLUMN public.menu_ref.me_code IS 'Menu Code ';
COMMENT ON COLUMN public.menu_ref.me_menu IS 'Label to display';
COMMENT ON COLUMN public.menu_ref.me_file IS 'if not empty file to include';
COMMENT ON COLUMN public.menu_ref.me_url IS 'url ';
COMMENT ON COLUMN public.menu_ref.me_type IS 'ME for menu
PR for Printing
SP for special meaning (ex: return to line)
PL for plugin';
COMMENT ON TABLE public.payment_method IS 'Contains the different media of payment and the corresponding ledger';
COMMENT ON COLUMN public.payment_method.jrn_def_id IS 'Ledger using this payment method';
COMMENT ON TABLE public.op_predef IS 'predefined operation';
COMMENT ON COLUMN public.op_predef.jrn_def_id IS 'jrn_id';
COMMENT ON COLUMN public.op_predef.od_name IS 'name of the operation';
COMMENT ON TABLE public.op_predef_detail IS 'contains the detail of predefined operations';
COMMENT ON TABLE public.operation_analytique IS 'History of the analytic account';
COMMENT ON COLUMN public.operation_analytique.oa_jrnx_id_source IS 'jrnx.j_id source of this amount, this amount is computed from an amount giving a ND VAT.Normally NULL  is there is no ND VAT.';
COMMENT ON COLUMN public.operation_analytique.oa_positive IS 'Sign of the amount';
COMMENT ON COLUMN public.operation_analytique.f_id IS 'FK to fiche.f_id , used only with ODS';
COMMENT ON TABLE public.operation_currency IS 'Information about currency';
COMMENT ON COLUMN public.operation_currency.oc_amount IS 'amount in currency';
COMMENT ON COLUMN public.operation_currency.oc_vat_amount IS 'vat amount in currency';
COMMENT ON COLUMN public.operation_currency.oc_price_unit IS 'unit price in currency';
COMMENT ON COLUMN public.operation_currency.j_id IS 'fk to jrnx';
COMMENT ON TABLE public.operation_tag IS 'Tag for operation';
COMMENT ON TABLE public.parameter IS 'parameter of the company';
COMMENT ON TABLE public.parameter_extra IS 'Extra parameter for the folder';
COMMENT ON COLUMN public.parameter_extra.pe_code IS 'Code used in the Document:generate';
COMMENT ON COLUMN public.parameter_extra.pe_label IS 'Label description';
COMMENT ON COLUMN public.parameter_extra.pe_value IS 'Value which will replace the tag in Document:generate';
COMMENT ON TABLE public.parm_code IS 'Parameter code and accountancy';
COMMENT ON TABLE public.parm_money IS 'Currency conversion';
COMMENT ON TABLE public.parm_periode IS 'Periode definition';
COMMENT ON COLUMN public.parm_periode.p_start IS 'Start date of periode';
COMMENT ON COLUMN public.parm_periode.p_end IS 'End date of periode';
COMMENT ON COLUMN public.parm_periode.p_exercice IS 'Exercice';
COMMENT ON COLUMN public.parm_periode.p_closed IS 'is closed';
COMMENT ON COLUMN public.parm_periode.p_central IS 'is centralized (obsolete)';
COMMENT ON COLUMN public.parm_periode.p_exercice_label IS 'label of the exercice';
COMMENT ON TABLE public.parm_poste IS 'Contains data for finding is the type of the account (asset)';
COMMENT ON TABLE public.plan_analytique IS 'Plan Analytique (max 5)';
COMMENT ON TABLE public.poste_analytique IS 'Poste Analytique';
COMMENT ON COLUMN public.poste_analytique.po_state IS 'Analytic Account state : 0 disabled 0 enabled ';
COMMENT ON TABLE public.profile IS 'Available profile ';
COMMENT ON COLUMN public.profile.p_name IS 'Name of the profile';
COMMENT ON COLUMN public.profile.p_desc IS 'description of the profile';
COMMENT ON COLUMN public.profile.with_calc IS 'show the calculator';
COMMENT ON COLUMN public.profile.with_direct_form IS 'show the direct form';
COMMENT ON COLUMN public.profile.with_search_card IS 'Display a icon for searching card : 1 display, 0 not displaid';
COMMENT ON TABLE public.profile_menu IS 'Join  between the profile and the menu ';
COMMENT ON COLUMN public.profile_menu.me_code_dep IS 'menu code dependency';
COMMENT ON COLUMN public.profile_menu.p_id IS 'link to profile';
COMMENT ON COLUMN public.profile_menu.p_order IS 'order of displaying menu';
COMMENT ON COLUMN public.profile_menu.p_type_display IS 'M is a module
E is a menu
S is a select (for plugin)';
COMMENT ON COLUMN public.profile_menu.pm_default IS 'default menu';
COMMENT ON COLUMN public.profile_menu.pm_id_dep IS 'parent of this menu item';
COMMENT ON TABLE public.profile_menu_type IS 'Type of menu';
COMMENT ON TABLE public.profile_mobile IS 'Menu for mobile device';
COMMENT ON COLUMN public.profile_mobile.pmo_id IS 'primary key';
COMMENT ON COLUMN public.profile_mobile.me_code IS 'Code of menu_ref to execute';
COMMENT ON COLUMN public.profile_mobile.pmo_order IS 'item order in menu';
COMMENT ON COLUMN public.profile_mobile.p_id IS 'Profile id ';
COMMENT ON COLUMN public.profile_mobile.pmo_default IS 'possible values are 1 , the default HTML header (javascript,CSS,...) is loaded ,  0  nothing is loaded from noalyss ';
COMMENT ON TABLE public.profile_sec_repository IS 'Available profile for user';
COMMENT ON COLUMN public.profile_sec_repository.ur_id IS 'pk';
COMMENT ON COLUMN public.profile_sec_repository.p_id IS 'fk to profile';
COMMENT ON COLUMN public.profile_sec_repository.r_id IS 'fk to stock_repository';
COMMENT ON COLUMN public.profile_sec_repository.ur_right IS 'Type of right : R for readonly W for write';
COMMENT ON TABLE public.profile_user IS 'Contains the available profile for users';
COMMENT ON COLUMN public.profile_user.user_name IS 'fk to available_user : login';
COMMENT ON COLUMN public.profile_user.p_id IS 'fk to profile';
COMMENT ON TABLE public.quant_fin IS 'Simple operation for financial';
COMMENT ON TABLE public.quant_purchase IS 'Supplemental info for purchase';
COMMENT ON COLUMN public.quant_purchase.qp_vat_sided IS 'amount of the VAT which avoid VAT, case of the VAT which add the same amount at the deb and cred';
COMMENT ON TABLE public.quant_sold IS 'Contains about invoice for customer';
COMMENT ON TABLE public.stock_change IS 'Change of stock';
COMMENT ON TABLE public.stock_goods IS 'About the goods';
COMMENT ON TABLE public.stock_repository IS 'stock repository';
COMMENT ON COLUMN public.stock_repository.r_id IS 'pk';
COMMENT ON COLUMN public.stock_repository.r_name IS 'name of the stock';
COMMENT ON COLUMN public.stock_repository.r_adress IS 'adress of the stock';
COMMENT ON COLUMN public.stock_repository.r_country IS 'country of the stock';
COMMENT ON COLUMN public.stock_repository.r_city IS 'City of the stock';
COMMENT ON COLUMN public.stock_repository.r_phone IS 'Phone number';
COMMENT ON TABLE public.tag_group IS 'Group of tags';
COMMENT ON COLUMN public.tag_group.tg_name IS 'Nom du groupe';
COMMENT ON TABLE public.tags IS 'Tags name';
COMMENT ON COLUMN public.tags.t_actif IS 'Y if the tag is activate and can be used ';
COMMENT ON TABLE public.tmp_pcmn IS 'Plan comptable minimum normalisé';
COMMENT ON COLUMN public.tmp_pcmn.id IS 'allow to identify the row, it is unique and not null (pseudo pk)';
COMMENT ON COLUMN public.tmp_pcmn.pcm_direct_use IS 'Value are N or Y , N cannot be used directly , not even through a card';
COMMENT ON TABLE public.todo_list IS 'Todo list';
COMMENT ON COLUMN public.todo_list.is_public IS 'Flag for the public parameter';
COMMENT ON TABLE public.todo_list_shared IS 'Note of todo list shared with other users';
COMMENT ON COLUMN public.todo_list_shared.todo_list_id IS 'fk to todo_list';
COMMENT ON COLUMN public.todo_list_shared.use_login IS 'user login';
COMMENT ON TABLE public.tva_rate IS 'Rate of vat';
COMMENT ON COLUMN public.tva_rate.tva_label IS 'Label';
COMMENT ON COLUMN public.tva_rate.tva_rate IS 'Rate';
COMMENT ON COLUMN public.tva_rate.tva_comment IS 'Description of VAT';
COMMENT ON COLUMN public.tva_rate.tva_poste IS 'accounting';
COMMENT ON COLUMN public.tva_rate.tva_both_side IS 'If set to 1 , the amount VAT will be reversed (autoliquidation)';
COMMENT ON COLUMN public.tva_rate.tva_payment_sale IS 'Check if the VAT on Sale  must be declared when at the date of payment (P) or the date of operation (O)';
COMMENT ON COLUMN public.tva_rate.tva_payment_purchase IS 'Check if the VAT on Purchase must be declared when at the date of payment (P) or the date of operation (O)';
COMMENT ON TABLE public.user_active_security IS 'Security for user';
COMMENT ON COLUMN public.user_active_security.us_login IS 'user''s login';
COMMENT ON COLUMN public.user_active_security.us_ledger IS 'Flag Security for ledger';
COMMENT ON COLUMN public.user_active_security.us_action IS 'Security for action';
COMMENT ON TABLE public.user_filter IS 'Filter for the search';
COMMENT ON COLUMN public.user_filter.operation_filter IS 'Status of the operation  : paid, unpaid or all operation';
COMMENT ON COLUMN public.user_filter.uf_tag IS 'Tag list';
COMMENT ON COLUMN public.user_filter.uf_tag_option IS '0 : all tags must be present, 1: at least one';
COMMENT ON COLUMN public.user_filter.uf_currency_code IS 'correspond to currency.id';
COMMENT ON COLUMN public.user_filter.tva_id_search IS 'VAT id ';
COMMENT ON TABLE public.user_local_pref IS 'The user''s local parameter ';
COMMENT ON COLUMN public.user_local_pref.user_id IS 'user''s login ';
COMMENT ON COLUMN public.user_local_pref.parameter_type IS 'the type of parameter ';
COMMENT ON COLUMN public.user_local_pref.parameter_value IS 'the value of parameter ';
COMMENT ON TABLE public.user_sec_act IS 'Security on actions for user';
COMMENT ON TABLE public.user_sec_action_profile IS 'Available profile for user';
COMMENT ON COLUMN public.user_sec_action_profile.ua_id IS 'pk';
COMMENT ON COLUMN public.user_sec_action_profile.p_id IS 'fk to profile';
COMMENT ON COLUMN public.user_sec_action_profile.ua_right IS 'Type of right : R for readonly W for write';
COMMENT ON TABLE public.user_sec_jrn IS 'Security on ledger for users';
COMMENT ON VIEW public.vw_fiche_attr IS 'Some attribute for all cards';
COMMENT ON VIEW public.v_menu_description IS 'Description des menus';
COMMENT ON VIEW public.v_menu_profile IS 'Give the profile and the menu + dependencies';
COMMENT ON TABLE public.version IS 'DB version';
COMMENT ON VIEW public.vw_fiche_def IS 'all the attributs for	card family';
COMMENT ON CONSTRAINT uniq_user_ledger ON public.user_sec_jrn IS 'Create an unique combination user / ledger';
COMMENT ON TRIGGER action_gestion_t_insert_update ON public.action_gestion IS 'Truncate the column ag_title to 70 char';
COMMENT ON TRIGGER profile_user_ins_upd ON public.profile_user IS 'Force the column user_name to lowercase';
COMMENT ON TRIGGER t_jrnx_ins ON public.jrnx IS 'check that the qcode used by the card exists and format it : uppercase and trim the space';
COMMENT ON TRIGGER t_letter_del ON public.jrnx IS 'Delete the lettering for this row';
COMMENT ON TRIGGER todo_list_ins_upd ON public.todo_list IS 'Force the column use_login to lowercase';
COMMENT ON TRIGGER todo_list_shared_ins_upd ON public.todo_list_shared IS 'Force the column ua_login to lowercase';
COMMENT ON TRIGGER user_sec_act_ins_upd ON public.user_sec_act IS 'Force the column ua_login to lowercase';
COMMENT ON TRIGGER user_sec_jrn_after_ins_upd ON public.user_sec_jrn IS 'Force the column uj_login to lowercase';
