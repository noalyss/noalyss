<?php
/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2019) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief called from p_id already set, 
 * @see action_document_type_mtable::input
 * @see 
 */
// SQL Object Document_Type
$table=$this->get_table();
// db connx
$cn=$table->cn;
?>

<?php echo _('Prochain numéro') ?>
<p class="info" style="display:inline">
    (
<?php echo _('numéro actuel') ?>
<?php
$last=0;
if ( $table->dt_id > 0) {
    $ret=$cn->get_array("select last_value,is_called from seq_doc_type_".$table->dt_id);

    $last=$ret[0]['last_value'];
    /* !
     * \note  With PSQL sequence , the last_value column is 1 when before   AND after the first call, to make the difference between them
     * I have to check whether the sequence has been already called or not */
    if ($ret[0]['is_called']=='f')
        $last--;
}
echo $last;
?>
    )
</p>

<?php
echo
Icon_Action::infobulle(15);
?>
<?php
$seq=new INum('seq', 0);
echo $seq->input();
?>
<div>
    <h3 class="info" sytle="margin-block: 4px"><?php echo _("Détail") ?></h3>
    <ul class="tab_row" style="padding-top: 0px">
        <li>
<?php
$i=new ICheckBox("detail_operation",1);
if ( Document_Option::is_enable_operation_detail($table->dt_id)) $i->set_check(1);else $i->set_check(0);
echo $i->input();
echo _("Détail opération");
$select_detail_operation=new ISelect("select_option_operation");
$select_detail_operation->value=array(["value"=>"VEN","label"=>_("Prix vente")],
                                      ["value"=>"ACH","label"=>_("Prix achat")]);
$select_detail_operation->set_value(Document_Option::option_operation_detail($table->dt_id));
echo $select_detail_operation->input();
?>
        </li> 
        <li>
<?php
$i=new ICheckBox("det_contact_mul",1);
if ( Document_Option::is_enable_contact_multiple($table->dt_id)) $i->set_check(1); else $i->set_check(0);
echo $i->input();
echo _("Autres fiches");
?>
        </li> 
        <li>
<?php
$i=new ICheckBox("make_invoice",1);
if ( Document_Option::is_enable_make_invoice($table->dt_id)) $i->set_check(1); else $i->set_check(0);
echo $i->input();
echo _("Création de facture");
?>
        </li> 
        <li>
<?php
$i=new ICheckBox("make_feenote",1);
if ( Document_Option::is_enable_make_feenote($table->dt_id)) $i->set_check(1); else $i->set_check(0);
echo $i->input();
echo _("Création de note de frais ou facture Achat");
?>
        </li>

        <li>
            <?php
            $description=new ICheckBox("editable_description",1);
            if ( Document_Option::is_enable_editable_description($table->dt_id)) $description->set_check(1); 
                else $description->set_check(0);
                
            echo $description->input();
            echo _("Description modifiable");
            ?>
        </li>
        <li>
<?php
$i=new ICheckBox("followup_comment",1);
if ( Document_Option::is_enable_comment($table->dt_id)) $i->set_check(1); else $i->set_check(0);
echo $i->input();
echo _("Commentaire");
$select_comment_type=new ISelect("select_comment");
$select_comment_type->value=array(["value"=>"ONE_EDIT","label"=>_("Unique")],
                                      ["value"=>"SOME_FIXED","label"=>_("Multiples")]);
$select_comment_type->set_value(Document_Option::option_comment($table->dt_id));
echo $select_comment_type->input();
?>
        </li> 
    <li>
        <?php
        $i=new ICheckBox("videoconf_server",1);
        $checked = (Document_Option::is_enable_video_conf($table->dt_id)==true) ?1:0;
        $i->set_check($checked);
        
        echo $i->input();
        echo _("Serveur de videoconf");
        $itVideoconfServer=new IText("videoconf_server_url");
        $itVideoconfServer->value=Document_Option::option_video_conf($table->dt_id);
        echo $itVideoconfServer->input();
        ?>
    </li>
    </ul>
</div>
<div>
     <h3 class="info" sytle="margin-block: 4px"><?php echo _("Options contact") ?></h3>
    <ul class="tab_row" style="padding-top: 0px">
<?php        
            
$nb_option=count($aOption);
for ($i=0;$i<$nb_option;$i++)
{
    echo '<li>';
    
    echo HtmlInput::hidden("cor_id[]", $aOption[$i]["cor_id"]);
    $is=new ICheckBox("contact_option$i",1);
    if ($aOption[$i]['jdoc_enable'] ==1 ) {
        $is->set_check(1);
    } else {
        $is->set_check(0);
    }
    echo $is->input();
    echo "&nbsp;&nbsp;&nbsp;"._($aOption[$i]['cor_label']);
    echo '</li>';
}
?>
</ul>
</div>
