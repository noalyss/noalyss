<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
$str_anc="";
global $g_parameter,$g_user;
?><?php require_once NOALYSS_TEMPLATE.'/ledger_detail_top.php'; ?>
<div class="content" style="padding:0;">
<?php
   $cn=Dossier::connect();
  $owner=new Noalyss_Parameter_Folder($cn);

?>
    <?php if ( $access=='W') : ?>
<form class="print" onsubmit="return op_save(this);">
   <?php endif; ?>

    <?php echo HtmlInput::hidden('whatdiv',$div).HtmlInput::hidden('jr_id',$jr_id).dossier::hidden();?>
            <table style="width:100%"><tr><td>
<table>
<tr>
<?php
$date=new IDate('p_date');
if (  $g_parameter->MY_STRICT=='Y' && $g_user->check_action(UPDDATE)==0) {
    $date->setReadOnly(true);
}
$date->value=format_date($obj->det->jr_date);
 echo td(_('Date')).td($date->input());

 ?>

</tr>

<tr>
<?php 
$bk=new Fiche($cn,$obj->det->array[0]['qf_bank']);
$view_card_detail=HtmlInput::card_detail($bk->get_quick_code(),h($bk->getName()), ' class="line" ');
echo td(_('Compte en banque')).td($view_card_detail).td();

?>
</tr>
<tr>
<?php 

$bk=new Fiche($cn,$obj->det->array[0]['qf_other']);
$view_card_detail=HtmlInput::card_detail($bk->get_quick_code(),h($bk->getName()), ' class="line" ');

echo td(_('Tiers')).td($view_card_detail);
?>
</tr>

<tr>
<?php 
  $itext=new IText('lib');
  $itext->value=(!empty($obj->det->jr_comment))?strip_tags($obj->det->jr_comment):'';
  $itext->size=40;
  echo td(_('Libellé')).td($itext->input(),' colspan="2" style="width:auto"');


?>
</tr>
<tr>
<?php echo td(_('montant')).td(nbm($obj->det->array[0]['qf_amount']),' class="inum"');?>
</tr>
<tr>
<?php 
$itext=new IText('npj');
$itext->value=(!empty($obj->det->jr_pj_number))?strip_tags($obj->det->jr_pj_number):'';
echo td(_('Pièce')).td($itext->input());
?>

</tr>
</table>
</td>
                <td style="width:50%;height:100%;vertical-align:top;text-align: center">
                    <table style="width:99%;height:8rem;vertical-align:top;">
                        <tr style="height: 5%">
                            <td style="text-align:center;vertical-align: top">
                                Note
                            </td></tr>
                        <tr>
                            <td style="text-align:center;vertical-align: top">
                                <?php
                                $inote = new ITextarea('jrn_note');
                                $inote->style=' class="itextarea" style="width:90%;height:100%;"';
                                $inote->value = strip_tags($obj->det->note);
                                echo $inote->input();
                                ?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="operation_tag_td<?=$div?>">
                                    <?php
                                                                       /******************************************************************************************************************
                                     * Tags on operation
                                     *****************************************************************************************************************/
                                    $tag_operation=new Tag_Operation($cn);
                                    $tag_operation->set_jrn_id($obj->det->jr_id);
                                    $tag_operation->tag_cell($div);
                                    ?>

                                </div>
                                
                                <?php
                                // Button add tags
                                if ( $access=='W') { echo Tag_Operation::button_search($obj->det->jr_id,$div);}
                                ?>
                             </td>
                        </tr>
                    </table>
                </td>

</tr>
</table>

</td>
<?php
/**
 * Display Currency in a column, if invoice not recorded in EUR
 */
if ($obj->det->currency_id!=0)
{
    $currency_amount=$obj->db->get_value("select  oc_amount from operation_currency where j_id =$1", 
            [$obj->det->array[0]['j_id']]);
    $currency_rate=$obj->db->get_value("select currency_rate from jrn where jr_id=$1",[$obj->jr_id]);
    $currency_code=$obj->db->get_value("select cr_code_iso from currency where id=$1",[$obj->det->currency_id]);
    printf (_("%s Taux utilisé %s Montant en devise %s"),$currency_code,$currency_rate,$currency_amount);
}
?>                    
<div class="myfieldset">
<?php 
  $detail=new Acc_Misc($cn,$obj->jr_id);
  $detail->get();
?>
<?php 
 if ($owner->MY_ANALYTIC != 'nu' /*&& $div == 'popup'*/){
      $anc=new Anc_Plan($cn);
      $a_anc=$anc->get_list(' order by pa_id ');
      $x=count($a_anc);
      /* set the width of the col */
      $str_anc .= '<th colspan="'.$x.'" style="width:auto;text-align:center">'._('Compt. Analytique').'</th>';

      /* add hidden variables pa[] to hold the value of pa_id */
      $str_anc .= Anc_Plan::hidden($a_anc);
    }
    bcscale(2);
  for ($e=0;$e<count($detail->det->array);$e++) {
    $row=''; $q=$detail->det->array;
    $fiche=new Fiche($cn);
    $fiche->get_by_qcode($q[$e]['j_qcode']);
   /* Analytic accountancy */
    if ( $owner->MY_ANALYTIC != "nu" /*&& $div == 'popup'*/)
      {
	$poste=$fiche->strAttribut(ATTR_DEF_ACCOUNT);
	if (  $g_parameter->match_analytic($q[$e]['j_poste']))
	  {
            $qcode=$fiche->strAttribut(ATTR_DEF_QUICKCODE);
	    $anc_op=new Anc_Operation($cn);
	    $anc_op->j_id=$q[$e]['j_id'];
	    $anc_op->in_div=$div;
            $str_anc.='<tr>';
            $str_anc.=td($poste);
            $str_anc.=td($qcode);
            $str_anc.=td(nbm($q[$e]['j_montant']));
            $str_anc.='<td>';
	    $str_anc.= HtmlInput::hidden('opanc[]',$anc_op->j_id);
            $montant=($q[$e]['j_debit'] == "t")?$q[$e]['j_montant']:bcmul($q[$e]['j_montant'], -1);
	    $str_anc.=$anc_op->display_table(1,$montant,$div);
            $str_anc.='</td>';
            $str_anc.='</tr>';

          } 
      }
  }
?>
</div>
<?php 
require_once NOALYSS_TEMPLATE.'/ledger_detail_bottom.php';
?>
</div>
