<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Copyright Author Dany De Bontridder danydb@aevalys.eu

/*!\file
 * \brief this file includes the called plugin. It  check first
 * the security. Load several javascript files
 */
require_once '../include/constant.php';
require_once NOALYSS_INCLUDE.'/class/database.class.php';
require_once NOALYSS_INCLUDE.'/class/dossier.class.php';
require_once NOALYSS_INCLUDE.'/lib/ac_common.php';
require_once NOALYSS_INCLUDE.'/lib/function_javascript.php';
require_once NOALYSS_INCLUDE.'/class/extension.class.php';
require_once  NOALYSS_INCLUDE.'/lib/html_input.class.php';
require_once NOALYSS_INCLUDE.'/lib/iselect.class.php';
require_once NOALYSS_INCLUDE . '/class/noalyss_user.class.php';

MaintenanceMode("block.html");

global $g_user,$cn,$g_parameter;
$cn=Dossier::connect();
$g_parameter=new Noalyss_Parameter_Folder($cn);

$http=new HttpInput();
$cn=Dossier::connect();
$g_user=new Noalyss_user($cn);
$g_user->check();
$only_plugin=$g_user->check_dossier(dossier::id());


set_language();
$ext=new Extension($cn);

if ( $ext->search($http->request("plugin_code")) != -1 )
  {
    /* security */
    if ( !isset ($_SESSION[SESSION_KEY.'g_user']) || $ext->can_request($_SESSION[SESSION_KEY.'g_user']) == 0 )
      {
		exit();
      }
    define ('ALLOWED',True);
    if ( LOGINPUT)
    {
      $file_loginput=fopen($_ENV['TMP'].'/plugin-export-'.$ext->me_code.'-'.$_SERVER['REQUEST_TIME'].'.php','a+');
      fwrite ($file_loginput,"<?php \n");
      fwrite ($file_loginput,'//@description: export  '.$ext->me_code."\n");
      fwrite($file_loginput, '$_GET='.var_export($_GET,true));
      fwrite($file_loginput,";\n");
      fwrite($file_loginput, '$_POST='.var_export($_POST,true));
      fwrite($file_loginput,";\n");
      fwrite($file_loginput, '$_POST[\'gDossier\']=$gDossierLogInput;');
      fwrite($file_loginput,"\n");
      fwrite($file_loginput, '$_GET[\'gDossier\']=$gDossierLogInput;');
      fwrite($file_loginput,"\n");
      fwrite($file_loginput,' $_REQUEST=array_merge($_GET,$_POST);');
      fwrite($file_loginput,"\n");
      fclose($file_loginput);
    }
    /* call the ajax script */
    require_once NOALYSS_PLUGIN.DIRECTORY_SEPARATOR.dirname(trim($ext->getp('me_file'))).DIRECTORY_SEPARATOR.'raw.php';
  }
else
  {
    alert(j(_("Cette extension n'existe pas ")));
    exit();
  }
?>
